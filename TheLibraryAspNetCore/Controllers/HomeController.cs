﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TheLibraryAspNetCore.Models;

namespace TheLibraryAspNetCore.Controllers
{
    public class HomeController : Controller
    {
        internal IAuthorManager authorManager;
        private List<Author> authorsCollection;
        public HomeController()
        {

            authorManager = new AuthorManager();
            authorsCollection = authorManager.GetAuthorCollection();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpGet]
        public ActionResult DeleteBook(int authorId, int bookId)
        {
            var author = authorManager.GetAuthorById(authorId);
            var book = authorManager.GetBookById(bookId);
            var newAuthor = new Author(author.FirstName, author.LastName, author.Id, book);
            return View("DeleteBook", newAuthor);
        }
        [HttpPost]
        public ActionResult DeleteBook(int bookId)
        {
            //var author = authorManager.GetAuthorById(authorId);
            if (authorManager.GetBookById(bookId) != null)
            {
                authorManager.DeleteBook(bookId);
            }
            return View("Index", authorsCollection);
        }


        public ActionResult Index()
        {
            return View(authorsCollection);
        }

        public ActionResult ViewBooks(int authorId)
        {
            var author = authorManager.GetAuthorById(authorId);
            return View(author);
        }

        public ActionResult NewBookAdded()
        {
            return View("Index ", authorManager.GetAuthorCollection());
        }

        [HttpGet]
        public ActionResult AddBook(int authorId)
        {
            ViewData["authorId"] = authorId;
            return View("AddBook");
        }
        public ActionResult Trouble()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddBook(int authorId, Book book)
        {
            if (ModelState.IsValid)
            {
                var author = authorManager.GetAuthorById(authorId);
                if (author != null)
                {
                    authorManager.AddNewBook(author, authorManager.GetNewBook(book.Title));
                    return View("ViewBooks", author);
                }
                else
                {
                    ViewBag.Message = "Can't find this author";
                    return View("AddBook");
                }
            }
            else
            {
                ViewBag.Message = "";
                return View("AddBook");
            }
        }
    }
}
