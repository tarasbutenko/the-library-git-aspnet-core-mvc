﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TheLibraryAspNetCore.Models
{
    public class Author 
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Id { get; set; }
        public Author(string firstName, string lastName, int id)
        {
            FirstName = firstName;
            LastName = lastName;
            Id = id;
        }
        public Author(string firstName, string lastName, int id, params Book[] bookArray)
        {
            FirstName = firstName;
            LastName = lastName;
            Id = id;
            BooksCollection = bookArray.ToList();
        }
        public Author()
        {}
        public List<Book> BooksCollection;
    }
}