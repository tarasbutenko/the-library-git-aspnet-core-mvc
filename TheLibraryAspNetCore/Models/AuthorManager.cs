﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TheLibraryAspNetCore.Models
{
    public class AuthorManager : IAuthorManager
    {

        public static List<Author> AuthorsCollection = new List<Author>
            {
             new Author("Rudolf", "Berghammer", 1,
                new Book("Semantik von Programmiersprachen", 1),
                new Book("Ordnungen", 2),
                new Book("Mathematik für Informatiker: Grundlegende Begriffe und Strukturen", 3)),
             new Author("Nikil", "Dutt", 2,
                new Book("Memory Issues in Embedded Systems-on-Chip: Optimizations and Exploration", 4)),
             new Author("Taras", "Butenko", 3,
                new Book("Some interesting stories", 5)),
             new Author("John", "Lennon", 4,
                new Book("Yesterday", 6))
          };
        
        public AuthorManager()
        {

        }
        
        public List<Book> GetBooksCollection()
        {
            var BooksCollection = new List<Book>();

            foreach (var author in AuthorsCollection)
            {
                BooksCollection.AddRange(GetBooksCollectionByAuthor(author));
            }
            return BooksCollection;
        }

        public bool AddAuthor(Author author)
        {
            if (author != null)
            {
                AuthorsCollection.Add(author);
                return true;
            }
            else { return false; }
        }

        public List<Author> GetAuthorCollection()
        {
            return AuthorsCollection;
        }


        public Author GetAuthorById(int authorId)
        {
            return GetAuthorCollection().FirstOrDefault(a => a.Id == authorId);
        }

        public Book GetNewBook(string title)
        {

            return new Book(title, ReturnNewBookId());
        }

        public int ReturnNewBookId()
        {
            var booksCollection = GetBooksCollection();
            int newBookId = booksCollection.Last().Id;
            do
            {
                newBookId++;
            } while (booksCollection.Any(book => book.Id == newBookId));
            return newBookId;
        }
        public Author GetAuthorByBookId(int bookId)
        {
            return GetAuthorCollection().FirstOrDefault(a => GetBooksCollectionByAuthor(a).Any(book => book.Id == bookId));
        }
        public bool DeleteBook(int deletedBookId)
        {
            var author = GetAuthorByBookId(deletedBookId);
            return GetBooksCollectionByAuthor(author).Remove(GetBookById(deletedBookId));
        }
        public Book GetBookById(int bookId)
        {
            return GetBooksCollection().FirstOrDefault(book => book.Id == bookId);
        }
        public void AddNewBook(Author author, Book book)
        {
            GetBooksCollectionByAuthor(author).Add(book);
        }
        public List<Book> GetBooksCollectionByAuthor(Author author)
        {
            return author.BooksCollection;
        }

    }
}