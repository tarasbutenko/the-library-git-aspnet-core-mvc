﻿using System.ComponentModel.DataAnnotations;

namespace TheLibraryAspNetCore.Models
{
    public class Book
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please, enter book title")]
        public string Title { get; set; }
        public Book(string title, int id)
        {
            Title = title;
            Id = id;
        }
        public Book()
        { }
    }
}