﻿using System.Collections.Generic;

namespace TheLibraryAspNetCore.Models
{
    public interface IAuthorManager
    {
        List<Book> GetBooksCollection();
        bool AddAuthor(Author author);
        List<Author> GetAuthorCollection();
        Book GetNewBook(string Title);
        int ReturnNewBookId();
        Author GetAuthorById(int authorId);
        Author GetAuthorByBookId(int bookId);
        bool DeleteBook(int deletedBookId);
        Book GetBookById(int bookId);
        void AddNewBook(Author author, Book book);
        List<Book> GetBooksCollectionByAuthor(Author author);
    }
}
