﻿
using System.Collections.Generic;

namespace TheLibraryAspNetCore.Models
{
    public interface IBooksManager
    {
        List<Book> GetBooksCollection();
        void AddNewBook(Book newBook);
        bool DeleteBook(int deletedBookId);

    }
}
